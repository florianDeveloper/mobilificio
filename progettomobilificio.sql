DROP DATABASE IF EXISTS lezione29;
CREATE DATABASE lezione29;
use lezione29;

CREATE TABLE categorie(
	categorieid INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nome Varchar(150) NOT NULL,
    descrizione text,
    codice VARCHAR(150) UNIQUE
);

 
 
CREATE TABLE prodotto(
prodottoId INTEGER AUTO_INCREMENT NOT NULL,
nome VARCHAR(150) NOT NULL ,
descrizione VARCHAR(250),
codiceProdotto VARCHAR(150) UNIQUE,
prezzoVendita FLOAT NOT NULL,
PRIMARY KEY (prodottoId)
);

create table prodotto_categ(
idProdotto  integer not null,
idCateg  integer not null,
PRIMARY KEY (idProdotto, idCateg),
FOREIGN KEY (idProdotto) REFERENCES prodotto(prodottoId)  ON DELETE CASCADE,
FOREIGN KEY (idCateg) REFERENCES categorie(categorieid) ON DELETE CASCADE
);	

INSERT INTO `categorie` (`categorieid`, `nome`, `descrizione`, `codice`) VALUES (1, 'boh', 'In sunt earum cum molestiae est ipsum laboriosam. Est consectetur sapiente aut sed ut quia ut rerum. Sed et ut aliquam numquam et. Eos sit nemo et accusamus at.', '555795');
INSERT INTO `categorie` (`categorieid`, `nome`, `descrizione`, `codice`) VALUES (2, 'BAGNO', 'Sint explicabo est minima odio iste id et. Consequatur adipisci totam nisi rem in magni cumque. Quia ipsum blanditiis laudantium in.', '90120733');
INSERT INTO `categorie` (`categorieid`, `nome`, `descrizione`, `codice`) VALUES (3, 'CUCINA', 'Minima consequuntur non porro commodi necessitatibus vero sapiente. Consequuntur exercitationem quaerat perspiciatis alias temporibus. Corrupti placeat magni laudantium vel iusto similique. Voluptatum velit ea reiciendis totam reiciendis nam consequuntur.', '435');
INSERT INTO `categorie` (`categorieid`, `nome`, `descrizione`, `codice`) VALUES (4, 'NON LO SO', 'Ducimus fugit delectus odit. Suscipit similique qui dolore.', '59063259');
INSERT INTO `categorie` (`categorieid`, `nome`, `descrizione`, `codice`) VALUES (5, 'MANSARDA', 'A saepe sed voluptates fugiat. Animi vitae nihil consequuntur ut autem culpa quis. Officia facilis ea aspernatur.', '940274');
INSERT INTO `categorie` (`categorieid`, `nome`, `descrizione`, `codice`) VALUES (6, 'FAI TU', 'Sint id sit sint dolorum consectetur eos ratione. A ipsa rerum qui at similique esse distinctio. Velit tenetur rerum vel voluptas. Neque cupiditate quia nobis nisi vel aut veritatis.', '');
INSERT INTO `categorie` (`categorieid`, `nome`, `descrizione`, `codice`) VALUES (7, 'Cecile Crest', 'Possimus aut non sed nihil sed. Nihil dolores itaque libero consequuntur. Non ad aperiam distinctio non non.', '1241432');
INSERT INTO `categorie` (`categorieid`, `nome`, `descrizione`, `codice`) VALUES (8, 'Tom Mews', 'Ad esse necessitatibus earum eos ex. Iure quas quae eveniet voluptate voluptas.', '309');
INSERT INTO `categorie` (`categorieid`, `nome`, `descrizione`, `codice`) VALUES (9, 'Lang Islands', 'Est non numquam vitae perspiciatis ut excepturi. Id nostrum cumque impedit et. Ipsa et ratione consequatur.', '97787');
INSERT INTO `categorie` (`categorieid`, `nome`, `descrizione`, `codice`) VALUES (10, 'Hagenes Fords', 'Aut assumenda cumque delectus. Enim non voluptate nostrum atque. Et ad numquam labore sint aperiam.', '60590422');


INSERT INTO prodotto (nome,descrizione,codiceProdotto,prezzoVendita) VALUES
("ARMADIO","SPETTACOLARE ARMADIO CHE TI PUO CONTENERE DENTRO","ARM-231321",232.43),
("SEDIA","SERVE PER SEDERSI","ARWW-231321",232.43),
("LAVATRICE","SPETTACOLARE OGGETTO PER FARE I LAVORI SPORCHI","LAV-231WW1",232.43),
("LAVANDINO","PUOI LAVARE TANTE COSE","A343-231321",232.43);
INSERT INTO prodotto_categ (idProdotto,idCateg) VALUES
(1,1),
(1,3),
(2,2),
(2,3),
(3,2),
(3,1),
(1,10);

DELETE FROM categorie  WHERE categorieid = 2;



select * FROM CATEGORIE;
SELECT* FROM PRODOTTO;
select * FROM prodotto_categ;
SELECT categorie.categorieid ,prodotto.prodottoId,categorie.nome,categorie.descrizione,categorie.codice ,prodotto_categ.idProdotto,prodotto_categ.idCateg,prodotto.nome
 FROM categorie
 JOIN prodotto_categ ON categorie.categorieid = prodotto_categ.idCateg
 JOIN prodotto ON prodotto_categ.idProdotto=prodotto.prodottoID
 where prodotto.prodottoId = 2;