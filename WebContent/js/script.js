/**
 * 
 */
$(document).ready(
	function() {
		stampaCategorie();
		

	}

);

function stampaCategorie() {

	$.ajax(
		{
			url: "http://localhost:8080/GestioneMobilificio/aggiornacategorie",
			method: "GET",
			success: function(risultato) {
				stampaDettagliCateg(risultato);
			},
			error: function(risultato) {
			}
		}
	);

};
function stampaDettagliCateg(arrayCateg) {
	$("#contenutoCategorie").html(" ");

	for (let i = 0; i < arrayCateg.length; i++) {
		$("#contenutoCategorie").append(stampaCateg(arrayCateg[i]));
	}

}
function stampaCateg(obj_categ) {
	let rigacateg = "";

	rigacateg = "<tr data-cod =\"" + obj_categ.Id + "\" >" +
		"<td>" + obj_categ.nome + "</td>" +
		"<td>" + obj_categ.descrizione + "</td>" +
		"<td>" + obj_categ.codice + "</td>" +
		"<td class= 'text-center'>" +
		"<button type='button' class='btn btn-danger' onclick='eliminaCateg(this)'>X</button>"
		+ "</tr>";
	return rigacateg;



}
function eliminaCateg(obj_Button){
	let getId=$(obj_Button).parent().parent().data("cod");
	
	$.ajax(
		{
			url:"http://localhost:8080/GestioneMobilificio/eliminacategoria",
			method:"POST",
			data:{
				getid :getId
			},
			success:function(risultato){
				stampaCategorie();
				
				
			},
			error:function(risultato){
				
			}
		}
	);
	
};