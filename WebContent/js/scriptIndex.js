/**
 * 
 */


/**
 * 
 */
$(document).ready(
	function() {
		stampaProdotti();


		$("#inserisci").click(
			function() {
				$("#modaleInserimento").modal("show");
			}
		);
		$("#modalInserisci").click(
					function(){
						let varnome = $("#nome").val();
						let varDescrizione = $("#descrizione").val();
						let varcodice = $("#codiceprodotto").val();
						let varPrezzo =$("#prezzovendita").val();
						let varcategorie =$("#categorie").val();
						

						$.ajax(
								{
									url: "http://localhost:8080/CorsoNTTAPI/inserisciprodotto",
									method: "POST",
									data: {
										input_nome: varnome,
										input_descrizione: varDescrizione,
										input_codice :varcodice,
										input_prezzoVendita : varPrezzo,
										input_categorie : varcategorie
									},
									success: function(risult){
										
										
										switch(risult.risultato){
										case "OK":
											aggiornaTabella();
											$("#modaleInserimento").modal("toggle");
											alert("Inserito con successo");
											break;
										case "ERRORE":
											alert("ERRORE DI INSERIMENTO");
											break;
										}
										
									},
									error: function(errore){
									
									}
									
								}
						);
						
					}
			);

	}

);

function stampaProdotti() {

	$.ajax(
		{
			url: "http://localhost:8080/GestioneMobilificio/trovaprodotti",
			method: "GET",
			success: function(risultato) {
				stampaDettagliProd(risultato);
			},
			error: function(risultato) {
				alert(risultato.RispostaAjax.dettagli);
			}
		}
	);

};
function stampaDettagliProd(arrayProd) {
	$("#contenutoprodotti").html(" ");

	for (let i = 0; i < arrayProd.length; i++) {
		$("#contenutoprodotti").append(stampaProd(arrayProd[i]));
	}

}
function stampaProd(obj_Prod) {
	let rigaProd = "";

	rigaProd = "<tr data-cod =\"" + obj_Prod.prodottoId + "\" >" +
		"<td>" + obj_Prod.nome + "</td>" +
		"<td>" + obj_Prod.descrizione + "</td>" +
		"<td>" + obj_Prod.codiceProdotto + "</td>" +
		"<td>" + obj_Prod.prezzoVendita + "</td>" +
		"<td>" + stampaCategCorr(obj_Prod.elencoCat) + "</td>" +

		"<td class= 'text-center'>" +
		"<button type='button' class='btn btn-danger' onclick='eliminaProd(this)'>X</button>"
		+ "</tr>";
	return rigaProd;



}

function stampaCategCorr(arraycateg) {
	let categoria = "";
	for (let i = 0; i < arraycateg.length; i++) {
		let catobj = arraycateg[i];
		categoria += catobj.nome + ",";
	}
	return categoria;

};

function eliminaProd(obj_Button) {
	let pescaId = $(obj_Button).parent().parent().data("cod");

	$.ajax(
		{
			url: "http://localhost:8080/GestioneMobilificio/eliminaprodotto",
			method: "POST",
			data: {
				productid: pescaId
			},
			success: function(risultato) {

				stampaProdotti();


			},
			error: function(risultato) {

			}
		}
	);

};