package lezione29.api.model;

import java.util.ArrayList;

public class Prodotto {
	private int prodottoId;
	private String nome;
	private String descrizione;
	private String codiceProdotto;
	private float prezzoVendita;
	private ArrayList<Categorie> elencoCat = new ArrayList<Categorie>();
	
	
	public Prodotto(String nome, String descrizione, String codiceProdotto, float prezzoVendita, ArrayList<Categorie> elencoCat) {
		this.nome = nome;
		this.descrizione = descrizione;
		this.codiceProdotto = codiceProdotto;
		this.prezzoVendita = prezzoVendita;
		this.elencoCat = elencoCat;
	}
	public Prodotto() {
		
	}
	
	public int getProdottoId() {
		return prodottoId;
	}
	public void setProdottoId(int prodottoId) {
		this.prodottoId = prodottoId;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getCodiceProdotto() {
		return codiceProdotto;
	}
	public void setCodiceProdotto(String codiceProdotto) {
		this.codiceProdotto = codiceProdotto;
	}
	public float getPrezzoVendita() {
		return prezzoVendita;
	}
	public void setPrezzoVendita(float prezzoVendita) {
		this.prezzoVendita = prezzoVendita;
	}
	public ArrayList<Categorie> getElencoCat() {
		return elencoCat;
	}
	public void setElencoCat(ArrayList<Categorie> elencoCat) {
		this.elencoCat = elencoCat;
	}
	@Override
	public String toString() {
		return "Prodotto [prodottoId=" + prodottoId + ", nome=" + nome + ", descrizione=" + descrizione
				+ ", codiceProdotto=" + codiceProdotto + ", prezzoVendita=" + prezzoVendita + ", elencoCat=" + elencoCat
				+ "]";
	}
	
	
	
}
