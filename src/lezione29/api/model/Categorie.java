package lezione29.api.model;

public class Categorie {
	private int Id;
	private String nome;
	private String descrizione;
	private String codice;
	
	public Categorie () {
		
	}
	public Categorie(String nome, String descrizione, String codice) {
		this.nome = nome;
		this.descrizione = descrizione;
		this.codice = codice;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	@Override
	public String toString() {
		return "Categorie [Id=" + Id + ", nome=" + nome + ", descrizione=" + descrizione + ", codice=" + codice + "]";
	}
	
	
	
	
	

}
