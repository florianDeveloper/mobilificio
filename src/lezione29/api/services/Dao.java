package lezione29.api.services;

import java.sql.SQLException;
import java.util.ArrayList;

public interface Dao<T> {
	
	T getById(int id) throws SQLException;
	
	ArrayList<T> findAll() throws SQLException;
	
	boolean delete(T obj) throws SQLException;
	
	
}
