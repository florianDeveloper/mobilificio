package lezione29.api.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

import lezione29.api.conessione.ConnettoreDB;
import lezione29.api.model.Categorie;
import lezione29.api.model.Prodotto;

public class ProdottoDao implements Dao <Prodotto>{

	@Override
	public Prodotto getById(int id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT prodottoId, nome, descrizione,codiceprodotto FROM prodotto WHERE prodottoid =? ";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);
		ResultSet risultato = ps.executeQuery();
		Prodotto temp = new Prodotto();
		while(risultato.next()) {
			temp.setProdottoId(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setDescrizione(risultato.getString(3));
			temp.setCodiceProdotto(risultato.getString(4));
		}
		return temp;
	}

	@Override
	public ArrayList<Prodotto> findAll() throws SQLException {
		ArrayList<Prodotto> elencoProd = new ArrayList<Prodotto>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT prodottoId,nome,descrizione,codiceProdotto,prezzoVendita from prodotto";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			Prodotto temp = new Prodotto();
			temp.setProdottoId(rs.getInt(1));
			temp.setNome(rs.getString(2));
			temp.setDescrizione(rs.getString(3));
			temp.setCodiceProdotto(rs.getString(4));
			temp.setPrezzoVendita(rs.getFloat(5));
			temp.setElencoCat(new CategorieDao().findCategorieCorrelateByIdProdotto(rs.getInt(1)));
			elencoProd.add(temp);
			
			
		}
		System.out.println(elencoProd);
		return elencoProd;
	}

	@Override
	public boolean delete(Prodotto obj) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		   
       	String query = "DELETE FROM prodotto  WHERE prodottoid = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, obj.getProdottoId());
       	int risultato = ps.executeUpdate();
       	if(risultato>0) {
       		return true;
       	}else {
       		return false;
       	}
	}

}
