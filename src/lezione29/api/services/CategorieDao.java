package lezione29.api.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import lezione29.api.conessione.ConnettoreDB;
import lezione29.api.model.Categorie;


public class CategorieDao implements Dao<Categorie> {

	@Override
	public Categorie getById(int id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT categorieid, nome, descrizione,codice FROM categorie WHERE categorieid =? ";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);
		ResultSet risultato = ps.executeQuery();
		Categorie temp = new Categorie();
		while(risultato.next()) {
			temp.setId(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setDescrizione(risultato.getString(3));
			temp.setCodice(risultato.getString(4));
		}
		return temp;
	}

	@Override
	public ArrayList<Categorie> findAll() throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		   
       	String query = "SELECT categorieid, nome, descrizione,codice FROM categorie";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	ArrayList<Categorie> elenco = new ArrayList<Categorie>();
       	while(risultato.next()){
       		Categorie temp = new Categorie();
       		temp.setId(risultato.getInt(1));
       		temp.setNome(risultato.getString(2));
       		temp.setDescrizione(risultato.getString(3));
       		temp.setCodice(risultato.getString(4));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public boolean delete(Categorie obj) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		   
       	String query = "DELETE FROM categorie  WHERE categorieid = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, obj.getId());
       	int risultato = ps.executeUpdate();
       	if(risultato>0) {
       		return true;
       	}else {
       		return false;
       	}
	}
	
	public ArrayList<Categorie> findCategorieCorrelateByIdProdotto(int IdProdotto) throws SQLException {
		
		ArrayList<Categorie> elencoCat = new ArrayList<Categorie>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query ="SELECT categorie.categorieid ,prodotto.prodottoId,categorie.nome,categorie.descrizione,categorie.codice ,prodotto_categ.idProdotto,prodotto_categ.idCateg,prodotto.nome"
				+ " FROM categorie"
				+ " JOIN prodotto_categ ON categorie.categorieid = prodotto_categ.idCateg"
				+ " JOIN prodotto ON prodotto_categ.idProdotto=prodotto.prodottoID"
				+ " where prodotto.prodottoId = ?";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1,IdProdotto);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			Categorie catTemp = new Categorie();
			catTemp.setId(rs.getInt(1));
			catTemp.setNome(rs.getString(3));
			catTemp.setDescrizione(rs.getString(4));
			catTemp.setCodice(rs.getString(5));
			
			elencoCat.add(catTemp);
			
		}
		return elencoCat;
		
	}

}
