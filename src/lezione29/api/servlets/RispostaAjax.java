package lezione29.api.servlets;

public class RispostaAjax{
	private String risultato;
	private String dettaglio;

	public RispostaAjax(String risultato, String dettaglio) {
		
		this.risultato = risultato;
		this.dettaglio = dettaglio;
	}
}