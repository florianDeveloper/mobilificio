package lezione29.api.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lezione29.api.model.Categorie;
import lezione29.api.model.Prodotto;
import lezione29.api.services.ProdottoDao;

/**
 * Servlet implementation class InserisciProdotto
 */
@WebServlet("/inserisciprodotto")
public class InserisciProdotto extends HttpServlet {
      
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String varnome= request.getParameter("input_titolo") != null?request.getParameter("input_titolo"):"";
		String vardescrizione= request.getParameter("input_descrizione")!= null?request.getParameter("input_descrizione"):"";
		String varCodice= request.getParameter("input_codice")!= null?request.getParameter("input_codice"):"";
		Float prezzovendita= request.getParameter("input_prezzoVendita")!= null? Float.parseFloat(request.getParameter("input_prezzoVendita")):-1;
		String categorie= request.getParameter("input_categorie")!= null?request.getParameter("input_categorie"):"";
		
		ProdottoDao prodD = new ProdottoDao();
		String arrCateg[] = categorie.split(",");
		ArrayList<Categorie> list = new ArrayList<Categorie>();
		for(int i=0;i<arrCateg.length;i++) {
			Categorie catTemp = new Categorie();
			list.add(null);
			
		}
		
		Prodotto prod = new Prodotto(varnome,vardescrizione,varCodice,prezzovendita,arrCateg);
		
		
		
	}

}
