package lezione29.api.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import lezione29.api.model.Categorie;
import lezione29.api.services.CategorieDao;

/**
 * Servlet implementation class AggiornaCategorie
 */
@WebServlet("/aggiornacategorie")
public class AggiornaCategorie extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategorieDao cd = new CategorieDao();
		
		try {
			ArrayList<Categorie> elencoCategorie= cd.findAll();
			String json1 = new Gson().toJson(elencoCategorie);
			out.print(json1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
