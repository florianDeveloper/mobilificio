package lezione29.api.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import lezione29.api.model.Prodotto;
import lezione29.api.services.ProdottoDao;

/**
 * Servlet implementation class EliminaProdotto
 */
@WebServlet("/eliminaprodotto")
public class EliminaProdotto extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		Integer varIdProdotto = request.getParameter("productid")!=null? Integer.parseInt(request.getParameter("productid")):-1;
		ProdottoDao pd = new ProdottoDao();
		Prodotto prod =null;
		try {
			if(varIdProdotto != -1) {
				prod =pd.getById(varIdProdotto);
				
				if(pd.delete(prod)) {
					pw.print(new Gson().toJson(new RispostaAjax("OK", "DELETE AVENUTA CON SUCCESSO")));
				}
				
			}else {
				pw.print(new Gson().toJson(new RispostaAjax("ERRORE","id passato non e valido")));
			}
		} catch (SQLException e) {
			pw.print(new Gson().toJson(new RispostaAjax("ERRORE",e.getMessage())));
		}
		
		
	}

}
