package lezione29.api.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import lezione29.api.model.Prodotto;
import lezione29.api.services.ProdottoDao;

/**
 * Servlet implementation class TrovaProdotti
 */
@WebServlet("/trovaprodotti")
public class TrovaProdotti extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter pw = response.getWriter();
		
		ProdottoDao prodDao = new ProdottoDao();
		ArrayList<Prodotto> elencoProd = new ArrayList<Prodotto>();
		try {
			elencoProd=prodDao.findAll();
			pw.print(new Gson().toJson(elencoProd));
			
			
		} catch (SQLException e) {
			pw.print(new Gson().toJson(new RispostaAjax("ERRORE",e.getMessage())));
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
