	package lezione29.api.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import lezione29.api.model.Categorie;
import lezione29.api.services.CategorieDao;

/**
 * Servlet implementation class EliminaCategoria
 * 
 */
@WebServlet("/eliminacategoria")
public class EliminaCategoria extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Integer varId = request.getParameter("getid") != null ? Integer.parseInt(request.getParameter("getid")):-1;
		PrintWriter out = response.getWriter();
		CategorieDao cd = new CategorieDao();
		try {
			Categorie categTemp = cd.getById(varId);
			if(cd.delete(categTemp)) {
				RispostaAjax ra = new RispostaAjax("ok", " ");
				out.println(new Gson().toJson(ra));
				
			}else {
				RispostaAjax ra = new RispostaAjax("ERRORE", "oggetto non eliminato");
				out.println(new Gson().toJson(ra));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		
		
		
	
		
		
		
	}

}
